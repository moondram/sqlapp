require 'test_helper'

class TrialControllersControllerTest < ActionController::TestCase
  setup do
    @trial_controller = trial_controllers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trial_controllers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trial_controller" do
    assert_difference('TrialController.count') do
      post :create, trial_controller: {  }
    end

    assert_redirected_to trial_controller_path(assigns(:trial_controller))
  end

  test "should show trial_controller" do
    get :show, id: @trial_controller
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trial_controller
    assert_response :success
  end

  test "should update trial_controller" do
    patch :update, id: @trial_controller, trial_controller: {  }
    assert_redirected_to trial_controller_path(assigns(:trial_controller))
  end

  test "should destroy trial_controller" do
    assert_difference('TrialController.count', -1) do
      delete :destroy, id: @trial_controller
    end

    assert_redirected_to trial_controllers_path
  end
end
