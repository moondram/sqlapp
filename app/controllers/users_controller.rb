class UsersController < ApplicationController
     
 #    skip_before_filter :verify_authenticity_token, :only => ['update','create','destroy']
    
  def create    

    @user = User.new(user_params)   

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created' }
        format.json { render json: @user }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end
     
  def index
#       @user = User.first
#       @insurance = Insurance.first
#       @user.insurances << @insurance
      # @user_amount = @user.insurances.first.insurance_amount
      #  @user_amount = Vehicle.first.users.first.login_id
       @user_amount = Vehicle.getfirst
       @user_record = User.first
       #render :json => @user_record
       respond_to do |format|  
            format.html  
            format.json { render json: @user_record }  
       end  
  end
     
  def show  
      @user = User.find(params[:id])  
      respond_to do |format|  
           format.html  
           format.json { render json: @user }  
      end  
  end
     
     
  # PUT /users/1
  # PUT /users/1.json
  def update
   set_user    
   respond_to do |format|
     if @user.update_attributes(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render json: @user }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
     
  def destroy
    set_user
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
     
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
         @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
         ActiveSupport::JSON.decode(params[:user])
    end
     
     
end