class InsurancesController < ApplicationController
 #   skip_before_filter :verify_authenticity_token, :only => ['update','create','destroy']
     
  def index
       @message = "hello from insurances!!"
  end

  def edit
  end

  def create
     @insurance = Insurance.new(insurance_params)   
     
     respond_to do |format|
          if @insurance.save
           format.html { redirect_to @insurance, notice: 'User was successfully created' }
           format.json { render json: @insurance }
      else
           format.html 
           format.json { render json: @insurance.errors, status: :unprocessable_entity }
      end
    end
      
  end
     
  def show
      set_insurance 
      respond_to do |format|  
           format.html  
           format.json { render json: @insurance }  
      end  
  end

  def new
  end

  
  def update
    set_insurance   
    respond_to do |format|
         if @insurance.update_attributes(insurance_params)
              format.html { redirect_to @insurance, notice: 'User was successfully updated.' }
              format.json { render json: @insurance }
         else
             format.html { render action: "edit" }
              format.json { render json: @insurance.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    set_insurance
    @insurance.destroy
    respond_to do |format|
      format.html { redirect_to insurances_url, notice: 'insurance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
     
     
    private
    # Use callbacks to share common setup or constraints between actions.
     def set_insurance
          @insurance = Insurance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
     def insurance_params
         ActiveSupport::JSON.decode(params[:insurance])
    end
  
     
end
