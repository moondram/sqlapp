class TrialControllersController < ApplicationController
  before_action :set_trial_controller, only: [:show, :edit, :update, :destroy]

  # GET /trial_controllers
  # GET /trial_controllers.json
  def index
    @trial_controllers = TrialController.all
  end

  # GET /trial_controllers/1
  # GET /trial_controllers/1.json
  def show
  end

  # GET /trial_controllers/new
  def new
    @trial_controller = TrialController.new
  end

  # GET /trial_controllers/1/edit
  def edit
  end

  # POST /trial_controllers
  # POST /trial_controllers.json
  def create
    @trial_controller = TrialController.new(trial_controller_params)

    respond_to do |format|
      if @trial_controller.save
        format.html { redirect_to @trial_controller, notice: 'Trial controller was successfully created.' }
        format.json { render :show, status: :created, location: @trial_controller }
      else
        format.html { render :new }
        format.json { render json: @trial_controller.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trial_controllers/1
  # PATCH/PUT /trial_controllers/1.json
  def update
    respond_to do |format|
      if @trial_controller.update(trial_controller_params)
        format.html { redirect_to @trial_controller, notice: 'Trial controller was successfully updated.' }
        format.json { render :show, status: :ok, location: @trial_controller }
      else
        format.html { render :edit }
        format.json { render json: @trial_controller.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trial_controllers/1
  # DELETE /trial_controllers/1.json
  def destroy
    @trial_controller.destroy
    respond_to do |format|
      format.html { redirect_to trial_controllers_url, notice: 'Trial controller was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trial_controller
      @trial_controller = TrialController.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trial_controller_params
      params[:trial_controller]
    end
end
