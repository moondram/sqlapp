class VehiclesController < ApplicationController
  #   skip_before_filter :verify_authenticity_token, :only => ['update','create','destroy']
     
  def index
       @message = "hello from vehicles!!"
  end

  def create    
    @vehicle = Vehicle.new(vehicle_params)   
#     puts params[:vehicle]
     respond_to do |format|
      if @vehicle.save
              format.html { redirect_to @vehicle, notice: 'User was successfully created' }
              format.json { render json: @vehicle }
      else
             format.html 
             format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end

  end
 
     
  def edit
  end

  def show
      set_vehicle 
      respond_to do |format|  
           format.html  
           format.json { render json: @vehicle }  
      end  
  end

  def new
  end

  def update
    set_vehicle
    respond_to do |format|
         if @vehicle.update_attributes(vehicle_params)
             format.html { redirect_to @vehicle, notice: 'User was successfully updated.' }
             format.json { render json: @vehicle }
         else
             format.html { render action: "edit" }
             format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
       
  end

  def destroy
    set_vehicle
    @vehicle.destroy
    respond_to do |format|
      format.html { redirect_to vehicles_url, notice: 'vehicle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
     
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
          @vehicle = Vehicle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
          ActiveSupport::JSON.decode(params[:vehicle])
    end
     
     
end
