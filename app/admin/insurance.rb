ActiveAdmin.register Insurance do
     permit_params :insurance_number

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    selectable_column
    id_column
    column :insurance_number
    column :insurance_provider
    column :begin_date
    column :end_date
    column :insurance_amount
       
    actions
  end
     
   filter :insurance_number

end
