json.array!(@trial_controllers) do |trial_controller|
  json.extract! trial_controller, :id
  json.url trial_controller_url(trial_controller, format: :json)
end
