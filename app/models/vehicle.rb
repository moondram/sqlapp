class Vehicle < ActiveRecord::Base
     has_many  :insurances
     has_many  :users,through: :insurances
     
     accepts_nested_attributes_for :users, :allow_destroy => true
     
     scope :getfirst, lambda { first.users.first.city }
end
