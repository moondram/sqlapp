class User < ActiveRecord::Base
     has_many  :insurances
     has_many  :vehicles, through: :insurances
     
    
end
