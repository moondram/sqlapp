# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150317144023) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "insurances", force: :cascade do |t|
    t.integer  "insurance_number",   limit: 4
    t.string   "insurance_provider", limit: 255
    t.datetime "begin_date"
    t.datetime "end_date"
    t.decimal  "insurance_amount",               precision: 10
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "vehicle_id",         limit: 4
    t.integer  "user_id",            limit: 4
  end

  create_table "trial_controllers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "login_id",         limit: 255
    t.string   "password",         limit: 255
    t.string   "name",             limit: 255
    t.string   "street_address_1", limit: 255
    t.string   "street_address_2", limit: 255
    t.string   "city",             limit: 255
    t.string   "state",            limit: 255
    t.string   "country",          limit: 255
    t.integer  "pincode",          limit: 4
    t.integer  "phone_number",     limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "vehicles", force: :cascade do |t|
    t.string   "vehicle_registration_number", limit: 255
    t.integer  "engine_number",               limit: 4
    t.string   "class_of_vehicle",            limit: 255
    t.string   "engine_size",                 limit: 255
    t.string   "horse_power",                 limit: 255
    t.string   "vehicle_type",                limit: 255
    t.string   "vehicle_model",               limit: 255
    t.integer  "month_of_manufacture",        limit: 4
    t.integer  "year_of_manufacture",         limit: 4
    t.integer  "number_of_cylinders",         limit: 4
    t.integer  "chassis_number",              limit: 4
    t.string   "type_of_body",                limit: 255
    t.string   "fuel_used",                   limit: 255
    t.string   "manufacturer_name",           limit: 255
    t.string   "wheel_base",                  limit: 255
    t.integer  "seating_capacity",            limit: 4
    t.string   "unladen_weight",              limit: 255
    t.string   "colour",                      limit: 255
    t.boolean  "modifications_done",          limit: 1
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

end
