# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user_data = User.create(login_id: 'firstuser@gmail.com', password: "password", name: "userone" , street_address_1: "first street", street_address_2: "second_street",city: "mumbai",state: "maharashtra",country: "india" , pincode: 400071,phone_number: 100100)
vehicle_data = Vehicle.create(vehicle_registration_number: "abcd", engine_number: 123456 , class_of_vehicle: "sedan" , engine_size: "large" , horse_power: "heavybhp" , vehicle_type: \
          "car", vehicle_model: "etios", month_of_manufacture: 10 , year_of_manufacture: 2015 , number_of_cylinders: 4 , chassis_number: 10 , type_of_body: "metallic", \
          fuel_used: "gasoline", manufacturer_name: "toyota", wheel_base: "10" , seating_capacity: 4 , unladen_weight: "1000kg" , colour: "white", modifications_done: true)


user_data.insurances.create(vehicle: vehicle_data,insurance_number: 1234,insurance_provider: "aig", begin_date: "2013-04-02 15:10:07" , end_date: "2014-04-02 15:10:07", insurance_amount: 10000 )



# +-----------------------------+--------------+------+-----+---------+----------------+                                                                                    
# | Field                       | Type         | Null | Key | Default | Extra          |                                                                                    
# +-----------------------------+--------------+------+-----+---------+----------------+                                                                                    
# | id                          | int(11)      | NO   | PRI | NULL    | auto_increment |                                                                                    
# | vehicle_registration_number | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | engine_number               | int(11)      | YES  |     | NULL    |                |                                                                                    
# | class_of_vehicle            | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | engine_size                 | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | horse_power                 | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | vehicle_type                | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | vehicle_model               | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | month_of_manufacture        | int(11)      | YES  |     | NULL    |                |                                                                                    
# | year_of_manufacture         | int(11)      | YES  |     | NULL    |                |                                                                                    
# | number_of_cylinders         | int(11)      | YES  |     | NULL    |                |                                                                                    
# | chassis_number              | int(11)      | YES  |     | NULL    |                |                                                                                    
# | type_of_body                | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | fuel_used                   | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | manufacturer_name           | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | wheel_base                  | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | seating_capacity            | int(11)      | YES  |     | NULL    |                |                                                                                    
# | unladen_weight              | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | colour                      | varchar(255) | YES  |     | NULL    |                |                                                                                    
# | modifications_done          | tinyint(1)   | YES  |     | NULL    |                |                                                                                    
# | created_at                  | datetime     | NO   |     | NULL    |                |                                                                                    
# | updated_at                  | datetime     | NO   |     | NULL    |                |                                                                                    
# | user_id                     | int(11)      | YES  |     | NULL    |                |                                                                                    
# +-----------------------------+--------------+------+-----+---------+----------------+ 

#  login_id         | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | password         | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | name             | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | street_address_1 | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | street_address_2 | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | city             | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | state            | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | country          | varchar(255) | YES  |     | NULL    |                |                                                                                                                      
# | pincode          | int(11)      | YES  |     | NULL    |                |                                                                                                                      
# | phone_number     | int(11)      | YES  |     | NULL    | 
# 
# id                 | int(11)       | NO   | PRI | NULL    | auto_increment |                                                                                                                   
# | insurance_number   | int(11)       | YES  |     | NULL    |                |                                                                                                                   
# | insurance_provider | varchar(255)  | YES  |     | NULL    |                |                                                                                                                   
# | begin_date         | datetime      | YES  |     | NULL    |                |                                                                                                                   
# | end_date           | datetime      | YES  |     | NULL    |                |                                                                                                                   
# | insurance_amount   | decimal(10,0) | YES  |     | NULL    |                |                                                                                                                   
# | created_at         | datetime      | NO   |     | NULL    |                |                                                                                                                   
# | updated_at         | datetime      | NO   |     | NULL    |                |                                                                                                                   
# | user_id            | int(11)       | YES  |     | NULL    |                |   
# 