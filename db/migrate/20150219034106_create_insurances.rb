class CreateInsurances < ActiveRecord::Migration
  def change
    create_table :insurances do |t|
      t.integer :insurance_number
      t.string :insurance_provider
      t.datetime :begin_date
      t.datetime :end_date
      t.decimal :insurance_amount

      t.timestamps null: false
    end
  end
end
