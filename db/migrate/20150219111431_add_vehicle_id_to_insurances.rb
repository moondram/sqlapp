class AddVehicleIdToInsurances < ActiveRecord::Migration
  def change
    add_column :insurances, :vehicle_id, :integer
  end
end
