class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :vehicle_registration_number
      t.integer :engine_number
      t.string :class_of_vehicle
      t.string :engine_size
      t.string :horse_power
      t.string :vehicle_type
      t.string :vehicle_model
      t.integer :month_of_manufacture
      t.integer :year_of_manufacture
      t.integer :number_of_cylinders
      t.integer :chassis_number
      t.string :type_of_body
      t.string :fuel_used
      t.string :manufacturer_name
      t.string :wheel_base
      t.integer :seating_capacity
      t.string :unladen_weight
      t.string :colour
      t.boolean :modifications_done

      t.timestamps null: false
    end
  end
end
